/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.transcript.sphinx.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.services.analyser.ProcessArgs;

/**
 * Just a simple class than can be used to generate input resources to be sent to the service.
 */
public class CreateExempleFromFiles {


	/**
	 * @param args
	 * @throws WebLabCheckedException
	 */
	public static void main(String[] args) throws WebLabCheckedException {
		List<Document> docList = null;
		docList = buildAudioDoc(new File("src/test/resources/other/ifyouwant.wav"), new File("src/test/resources/digits/testJD16k.wav"));
		for (int i = 0; i < docList.size(); i++) {
			Document docTestGD = docList.get(i);
			ProcessArgs processArgs = new ProcessArgs();
			processArgs.setResource(docTestGD);
			WebLabMarshaller marshaller = new WebLabMarshaller();
			marshaller.marshalResource(docTestGD, new File("target", "output" + i + ".xml"));
		}

	}


	protected static List<Document> buildAudioDoc(File... audioFiles) throws WebLabCheckedException {
		List<Document> ret = new ArrayList<>();
		for (File audioFile : audioFiles) {
			Document document = WebLabResourceFactory.createResource("sphinxTest", "test-" + audioFile.getName(), Document.class);
			Audio audio = WebLabResourceFactory.createAndLinkMediaUnit(document, Audio.class);
			ContentManager.getInstance().writeNormalisedContent(audioFile, audio);
			ret.add(document);
		}
		return ret;

	}

}
