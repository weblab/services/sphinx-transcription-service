/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.transcript.sphinx.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;

/**
 * This class is a simple example that directly calls Sphinx without any dependence to the WebLab APIs.
 */
public class Proto {


	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("Loading models...");
		long tic = System.currentTimeMillis();
		Configuration configuration = new Configuration();

		// new one
		configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/acoustic/wsj");
		configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/acoustic/wsj/dict/cmudict.0.6d");
		configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/language/en-us.lm.dmp");


		long toc = System.currentTimeMillis();

		System.out.println("Models loaded in " + (toc - tic) + "ms");

		transcribeFileStream(configuration);

		// liveTranscribe(configuration);
	}


	protected static void liveTranscribe(Configuration configuration) throws IOException {
		LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);
		// Start recognition process pruning previously cached data.
		recognizer.startRecognition(true);
		recognizer.getResult();
		// Pause recognition process. It can be resumed then with
		// startRecognition(false).
		recognizer.stopRecognition();
	}


	protected static void transcribeFileStream(Configuration configuration) throws IOException, MalformedURLException, FileNotFoundException {
		long tic;
		long toc;
		StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);

		tic = System.currentTimeMillis();
		try (FileInputStream stream = new FileInputStream("src/test/resources/other/ifyouwant.wav")) {
			recognizer.startRecognition(stream);

			SpeechResult result;

			StringBuffer text = new StringBuffer();

			while ((result = recognizer.getResult()) != null) {
				System.out.println("Result :" + result.getHypothesis());
				text.append(' ');
				text.append(result.getHypothesis());
			}

			toc = System.currentTimeMillis();

			System.out.println("####################\nRecognition done in " + (toc - tic) + "ms");

			System.out.println("Stop Recognition. Final hypothesis: " + text);

			recognizer.stopRecognition();
		}
	}

}
