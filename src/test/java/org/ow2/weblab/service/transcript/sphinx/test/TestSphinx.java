/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.transcript.sphinx.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.TextUtil;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.service.transcript.sphinx.ATranscriptCreator;
import org.ow2.weblab.service.transcript.sphinx.SphinxTranscriber;
import org.ow2.weblab.service.transcript.sphinx.SphinxTranscriptorService;
import org.ow2.weblab.service.transcript.sphinx.WebLabTextTranscriptCreator;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

import edu.cmu.sphinx.api.Configuration;

/**
 * Testing the service with various inputs and various configurations.
 */
public class TestSphinx {


	@Test
	public void testSphinxWithDetails() throws Exception {
		final Configuration configuration = new Configuration();
		configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/acoustic/wsj");
		configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/acoustic/wsj/dict/cmudict.0.6d");
		configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/language/en-us.lm.dmp");
		final ATranscriptCreator transcriptor = new WebLabTextTranscriptCreator(new SphinxTranscriber(configuration), "en", true, false, true, true, true);

		this.realTestMethod(new SphinxTranscriptorService(transcriptor), "detailed", true);
	}


	@Test
	public void testSphinxSimpleOutput() throws Exception {
		this.realTestMethod(new XmlBeanFactory(new FileSystemResource("src/main/webapp/WEB-INF/cxfBeanFile.xml")).getBean(SphinxTranscriptorService.class), "simple-cxf", false);
	}


	@Test
	public void testSphinxSimpleCustomOutput() throws Exception {
		final Configuration configuration = new Configuration();
		configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/acoustic/wsj");
		configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/acoustic/wsj/dict/cmudict.0.6d");
		configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/language/en-us.lm.dmp");

		final ATranscriptCreator transcriptor = new WebLabTextTranscriptCreator(new SphinxTranscriber(configuration, "src/test/resources/testConfig.xml"), "en");

		this.realTestMethod(new SphinxTranscriptorService(transcriptor), "simple-custom", false);
	}


	private void realTestMethod(final SphinxTranscriptorService transcriber, final String name, final boolean detailed) throws Exception {
		final List<Document> docList = this.buildAudioDoc(new File("src/test/resources/other/10001-90210-01803.wav"), new File("src/test/resources/other/ifyouwant.wav")

		// , new File("src/test/resources/digits/testGD16k.wav"), new File("src/test/resources/digits/testJD16k.wav")
		// , new File("src/test/resources/fromVideo/ifYouWantToLearnAboutGiantTsunamis.wav"), new File("src/test/resources/fromVideo/soThatsHuge.wav")
		// , new File("src/test/resources/other/baker_mod.wav"), new File("src/test/resources/other/bush_mod.wav"), new File("src/test/resources/other/test.wav")

				);
		long totalDuration = 0;
		int i = 0;
		for (final Document doc : docList) {

			final ProcessArgs processArgs = new ProcessArgs();
			processArgs.setResource(doc);
			final long start = System.currentTimeMillis();
			transcriber.process(processArgs);
			final long duration = System.currentTimeMillis() - start;
			new WebLabMarshaller().marshalResource(doc, new File("target", name + "-" + i++ + "-" + duration + ".xml"));
			totalDuration += duration;
		}

		LogFactory.getLog(this.getClass()).info(name + " processed " + i + " files in " + totalDuration + " ms.");
		this.assertTextTranscripted("one zero zero zero one nine oh two one oh cyril one eight zero three", docList.get(0), 1, detailed);

		this.assertTextTranscripted("you want our about giants and not", docList.get(1), 1, detailed);
	}


	private List<Document> buildAudioDoc(final File... audioFiles) throws WebLabCheckedException {
		final List<Document> ret = new ArrayList<>();
		for (final File audioFile : audioFiles) {
			final Document document = WebLabResourceFactory.createResource("sphinxTest", "test-" + audioFile.getName(), Document.class);
			final Audio audio = WebLabResourceFactory.createAndLinkMediaUnit(document, Audio.class);
			ContentManager.getInstance().writeNormalisedContent(audioFile, audio);
			ret.add(document);
		}
		return ret;
	}


	private void assertTextTranscripted(final String expectedTranscript, final Document document, final int index, final boolean detailed) throws WebLabCheckedException {

		Assert.assertNotNull("No text section generated by transcription.", document.getMediaUnit().get(index));
		Assert.assertEquals("Generated media unit must be a text unit.", Text.class, document.getMediaUnit().get(1).getClass());
		final Text text = (Text) document.getMediaUnit().get(1);

		// System.out.println("Text transcribed: " + text.getContent().trim().replaceAll("\\s", " "));

		Assert.assertEquals("Transcripted text is false.", expectedTranscript, text.getContent().trim().replaceAll("<\\w+>", " ").replaceAll("\\+\\+.\\w+\\+\\+", " ").replaceAll("\\s+", " ").trim());

		final String[] transcriptedWords = expectedTranscript.split("\\s");
		if (detailed) {
			Assert.assertEquals("Number of transcripted segment is wrong.", transcriptedWords.length, text.getSegment().size());
			for (int i = 0; i < transcriptedWords.length; i++) {
				Assert.assertEquals(transcriptedWords[i], TextUtil.getSegmentText(text, (LinearSegment) text.getSegment().get(i)));
			}
		} else {
			Assert.assertEquals("Number of transcripted segment is wrong.", 0, text.getSegment().size());
		}
	}

}
