/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.transcript.sphinx;

import java.io.IOException;
import java.io.InputStream;

import edu.cmu.sphinx.api.AbstractSpeechRecognizer;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.Context;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import edu.cmu.sphinx.util.TimeFrame;



/**
 * This class is almost a copy past of the {@link StreamSpeechRecognizer} class from Sphinx.
 * We need the {@link Context} based constructor from {@link AbstractSpeechRecognizer} however, {@link StreamSpeechRecognizer} is hidding it.
 *
 * @author ymombrun
 * @date 2015-02-10
 * @see StreamSpeechRecognizer
 */
public class ConfigurableSpeechRecognizer extends AbstractSpeechRecognizer {


	/**
	 * Constructs new Configurable recognizer, only based on the simple {@link Configuration} object.
	 *
	 * @param context
	 *            The context of configuration to be used.
	 * @throws IOException
	 *             If an error occurs accessing the referenced files (xml/models/configuration..)
	 */
	public ConfigurableSpeechRecognizer(final Context context) throws IOException {
		super(context);
	}


	/**
	 * Starts recognition process on the provided audio stream.
	 *
	 * @param stream
	 *            The audio stream to be recognised from start to end
	 * @see #stopRecognition() That must be called once results have been consummed.
	 */
	public void startRecognition(final InputStream stream) {
		this.startRecognition(stream, TimeFrame.INFINITE);
	}


	/**
	 * Starts recognition process on the provided audio stream.
	 *
	 * @param stream
	 *            The audio stream to be recognised within the time frame
	 * @param timeFrame
	 *            Specify a segment of the stream to be analysed.
	 * @see #stopRecognition()
	 */
	public void startRecognition(final InputStream stream, final TimeFrame timeFrame) {
		this.recognizer.allocate();
		this.context.setSpeechSource(stream, timeFrame);
	}


	/**
	 * Stops recognition process.
	 *
	 * Must be called in order to release the resources.
	 */
	public void stopRecognition() {
		this.recognizer.deallocate();
	}

}
