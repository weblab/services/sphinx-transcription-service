/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.transcript.sphinx;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.RDF;
import org.ow2.weblab.core.extended.ontologies.RDFS;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.PoKHelper;
import org.ow2.weblab.core.helper.impl.JenaPoKHelper;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.purl.dc.elements.DublinCoreAnnotator;

import edu.cmu.sphinx.linguist.dictionary.Word;
import edu.cmu.sphinx.result.WordResult;

/**
 * Class in charge of extracting the audio files from input Document in order to create text and annotate the document.
 * It has one main method {@link #transcribeDocument(Document)} and a few methods inside that can be overridden in case of specific behaviour on a given project (format of annotations, kind of
 * incoming
 * files...)
 */
public class WebLabTextTranscriptCreator extends ATranscriptCreator {


	/**
	 * URI used in isProducedBy property
	 */
	private static final String SPHINX_SERVICE_URI = "http://weblab.ow2.org/services/speech2text#sphinx";


	private static final String SPHINX_URI = "http://www.speech.cs.cmu.edu/sphinx/ontology#";


	private static final String SPHINX_URI_SPELLING = WebLabTextTranscriptCreator.SPHINX_URI + "spelling";


	private static final String SPHINX_URI_SCORE = WebLabTextTranscriptCreator.SPHINX_URI + "score";


	private static final String SPHINX_URI_PRONUNCIATION = WebLabTextTranscriptCreator.SPHINX_URI + "pronunciation";


	private static final String SPHINX_URI_INSTANCE = WebLabTextTranscriptCreator.SPHINX_URI + "instance";


	private static final String SPHINX_URI_TOKEN_CLASS = WebLabTextTranscriptCreator.SPHINX_URI + "Token";


	private static final DecimalFormat scoreFmt = new DecimalFormat("0.00E00");


	protected final boolean writeTokenInfo;


	protected final boolean writeFiller;


	protected final boolean writeScore;


	protected final boolean writePronunciation;


	protected final boolean localiseWords;


	protected final String targetLang;


	/**
	 * Simple constructor used to generate minimal information.
	 *
	 * @param sphinxTranscriber
	 *            model used to transcript
	 * @param targetLang
	 *            target language
	 */
	public WebLabTextTranscriptCreator(final SphinxTranscriber sphinxTranscriber, final String targetLang) {
		this(sphinxTranscriber, targetLang, false, false, false, false, false);
	}


	/**
	 * Generates all text transcript from all audio media unit contained in a WebLab document using a specific sphinxTranscriptor
	 *
	 * @param sphinxTranscriber
	 *            model used to transcript
	 * @param targetLang
	 *            target language
	 * @param writeTokenInfo
	 *            true if your want to add extra information on each token
	 * @param writeFiller
	 *            true if you want filler words
	 * @param writeScore
	 *            true if you want to write score
	 * @param writePronunciation
	 *            true if you want to write pronunciation
	 * @param localiseWords
	 *            <code>true</code> if you want to create segments to localise each word
	 *
	 */
	public WebLabTextTranscriptCreator(final SphinxTranscriber sphinxTranscriber, final String targetLang, final boolean writeTokenInfo, final boolean writeFiller, final boolean writeScore,
			final boolean writePronunciation, final boolean localiseWords) {
		super(sphinxTranscriber);
		this.targetLang = targetLang;
		this.writeTokenInfo = writeTokenInfo;
		this.writeFiller = writeFiller;
		this.writeScore = writeScore;
		this.writePronunciation = writePronunciation;
		this.localiseWords = localiseWords;
	}


	@Override
	protected void transcribeFile(final File file, final MediaUnit mediaUnit, final Document document) throws UnexpectedException {

		/*
		 * creates transcribed Text media unit and links it to current Audio
		 */
		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
		final WProcessingAnnotator textProcessingAnnotator = new WProcessingAnnotator(text);
		new DublinCoreAnnotator(text).writeLanguage(this.targetLang);

		textProcessingAnnotator.writeTranscriptOf(URI.create(mediaUnit.getUri()));
		textProcessingAnnotator.writeProducedBy(URI.create(WebLabTextTranscriptCreator.SPHINX_SERVICE_URI));

		/*
		 * creates annotation unit
		 */
		final Annotation textAnnotation = WebLabResourceFactory.createAndLinkAnnotation(text);
		final Annotation audioAnnotation = WebLabResourceFactory.createAndLinkAnnotation(mediaUnit);

		/*
		 * for each transcribed text section for this audio file
		 */
		final List<List<WordResult>> tokens;
		try (final FileInputStream fis = new FileInputStream(file)) {
			tokens = this.sphinxTranscriber.transcribe(fis, this.writeFiller);
		} catch (final Exception e) {
			final String message = "An error occured while trying to process file " + file + ".";
			this.log.error(message, e);
			throw ExceptionFactory.createUnexpectedException(message, e);

		}
		for (final List<WordResult> sentence : tokens) {
			this.generatedTranscriptText(sentence, text, mediaUnit, textAnnotation, audioAnnotation);
		}

		if (text.isSetContent()) {
			text.setContent(text.getContent().trim());
		}
		if (!text.isSetContent() || text.getContent().isEmpty()) {
			this.log.debug("Nothing transcribed for mediaUnit " + mediaUnit.getUri() + " and file " + file.getAbsolutePath() + ".");
			document.getMediaUnit().remove(text);
			mediaUnit.getAnnotation().remove(audioAnnotation);
		}

	}


	/**
	 * @param document
	 *            The top level input document
	 * @return A map of MediaUnit to be annotated (often audio but might be video in subclasses).
	 * @throws ContentNotAvailableException
	 *             If content to be transcribed does not exist on a given mediaunit.
	 */
	@Override
	protected Map<MediaUnit, File> retrieveMapOfMediaUnit(final Document document) throws ContentNotAvailableException {
		final Map<MediaUnit, File> mediaUnits = new HashMap<>();
		for (final Audio audio : ResourceUtil.getSelectedSubResources(document, Audio.class)) {

			final File normalisedAudioFile;
			try {
				normalisedAudioFile = this.contentManager.readNormalisedContent(audio);
			} catch (final WebLabCheckedException wlce) {
				final String message = "Error during content retrieving on " + audio.getUri() + ".";
				this.log.error(message, wlce);
				throw ExceptionFactory.createContentNotAvailableException(message, wlce);
			}

			mediaUnits.put(audio, normalisedAudioFile);
		}

		return mediaUnits;
	}


	/**
	 * Creates aligned segments from Token, Text and Audio. inspired from Sphinx code in Result.getTimedWordPath
	 *
	 * @param token
	 *            Sphinx Token
	 * @param text
	 *            Text media unit transcribed
	 * @param mediaUnit
	 *            Audio source media unit
	 * @param audioAnnotation
	 *            Annotation on Audio unit
	 * @param textAnnotation
	 *            Annotation on Text unit
	 */
	protected void generatedTranscriptText(final List<WordResult> sentence, final Text text, final MediaUnit mediaUnit, final Annotation textAnnotation, final Annotation audioAnnotation) {

		final StringBuffer transcribedBuffer = new StringBuffer();

		for (final WordResult token : sentence) {
			final Word word = token.getWord();

			if (this.writeFiller || !word.isFiller()) {
				this.addWord(transcribedBuffer, textAnnotation, audioAnnotation, text, mediaUnit, token, token.getTimeFrame().getStart(), token.getTimeFrame().getEnd());
			}
		}


		final String transcribedString = transcribedBuffer.toString().trim();
		/*
		 * nothing to write
		 */
		if (transcribedString.length() > 0) {
			if ((text.getContent() != null) && (text.getContent().length() > 0)) {
				text.setContent(text.getContent() + transcribedString + "\n");
			} else {
				text.setContent(transcribedString + "\n");
			}
			// if we did not write any extra info, then audio Annotation should be removed
			if (!(this.writeFiller || this.writePronunciation || this.writeScore || this.writeTokenInfo || this.localiseWords)) {
				mediaUnit.getAnnotation().remove(audioAnnotation);
			}
		}

	}


	/**
	 * Generates the two segments with all configured annotations
	 *
	 * @param textAnnotation
	 *            Annotation where to add text segment metadata
	 * @param audioAnnotation
	 *            Annotation where to add audio segment metadata
	 * @param text
	 *            Text unit to add linear segment to
	 * @param mediaUnit
	 *            Audio unit to add temporal segment to
	 * @param token
	 *            the Sphinx recognised token
	 * @param previousWordFeature
	 *            Latest working feature of previous word
	 * @param previousFeature
	 *            Previous feature, just before that word
	 * @return The length of the added String
	 */
	protected int addWord(final StringBuffer sb, final Annotation textAnnotation, final Annotation audioAnnotation, final Text text, final MediaUnit mediaUnit, final WordResult wordResult,
			final long startTime, final long endTime) {
		if ((startTime < 0) || (startTime > endTime) || (endTime > Integer.MAX_VALUE)) {
			this.log.warn("Trouble with start and end time: " + startTime + " " + endTime);
			return 0;
		}

		final Word word = wordResult.getWord();

		/*
		 * linear segment
		 */
		int startPosition = sb.length();
		if (text.getContent() != null) {
			startPosition += text.getContent().length();
		}

		/*
		 * add the word to the buffer
		 */
		sb.append(word.getSpelling() + " ");

		if (this.localiseWords) {
			final TemporalSegment temporalSegment = SegmentFactory.createAndLinkSegment(mediaUnit, TemporalSegment.class);
			temporalSegment.setStart(Long.valueOf(startTime).intValue());
			temporalSegment.setEnd(Long.valueOf(endTime).intValue());

			final int endPosition = startPosition + word.getSpelling().length();
			final LinearSegment linearSegment = SegmentFactory.createAndLinkLinearSegment(text, startPosition, endPosition);

			/*
			 * segments alignment and annotation
			 */
			final URI linearSegmentUri = URI.create(linearSegment.getUri());
			final URI temporalSegmentUri = URI.create(temporalSegment.getUri());

			/*
			 * generates pokH on the audio annotation
			 */
			final PoKHelper audioPoKHelper = new JenaPoKHelper(audioAnnotation, false);

			/*
			 * generates unique temp uri for the token item
			 */
			final String tokenUri = WebLabTextTranscriptCreator.SPHINX_URI_INSTANCE + System.nanoTime();

			/*
			 * get meta data values
			 */
			final String score = WebLabTextTranscriptCreator.scoreFmt.format(wordResult.getScore());
			final String pronuniation = word.getMostLikelyPronunciation().toString();
			final String spelling = word.getSpelling();

			if (this.writeTokenInfo) {
				audioPoKHelper.createResStat(temporalSegment.getUri(), WebLabProcessing.IS_DELIMITER_OF_TRANSCRIPT_UNIT, text.getUri());
				audioPoKHelper.createResStat(tokenUri, RDF.TYPE, WebLabTextTranscriptCreator.SPHINX_URI_TOKEN_CLASS);
				audioPoKHelper.createLitStat(tokenUri, WebLabTextTranscriptCreator.SPHINX_URI_SPELLING, spelling);
				audioPoKHelper.createLitStat(tokenUri, RDFS.LABEL, spelling + " [" + pronuniation + " ] @ (" + score + ")");
			}

			if (this.writeScore) {
				audioPoKHelper.createLitStat(tokenUri, WebLabTextTranscriptCreator.SPHINX_URI_SCORE, score);
			}

			if (this.writePronunciation) {
				audioPoKHelper.createLitStat(tokenUri, WebLabTextTranscriptCreator.SPHINX_URI_PRONUNCIATION, pronuniation);
			}

			audioPoKHelper.commit();

			final WProcessingAnnotator textAnnotator = new WProcessingAnnotator(linearSegmentUri, textAnnotation);
			textAnnotator.writeGeneratedFrom(temporalSegmentUri);
		}

		return word.getSpelling().length() + 1;
	}



	@Override
	public String toString() {
		return "WebLabTextTranscriptor writing (filler, score, pronunciation): " + this.writeFiller + ", " + this.writeScore + ", " + this.writePronunciation + " using " + this.sphinxTranscriber
				+ " as transcriptor and " + this.contentManager + " as content manager.";
	}

}