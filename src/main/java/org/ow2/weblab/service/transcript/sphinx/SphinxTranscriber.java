/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.transcript.sphinx;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.Context;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.result.WordResult;

/**
 * This class is a merge of the two previous version of "Sphinx Transcriptor, sic".
 *
 * @author ymombrun, gdupont, jdoucy
 */
public class SphinxTranscriber {


	private static final String JAVA_UTIL_LOGGING_CONFIG_FILE = "java.util.logging.config.file";


	protected final Log log;


	protected final ConfigurableSpeechRecognizer recognizer;


	/**
	 * Instantiating the recogniser directly from the models paths
	 *
	 * @param configuration
	 *            The configuration object to be used in order to create the recognizer.
	 *
	 * @throws IOException
	 *             If an error occurred when trying to load the configuration
	 */
	public SphinxTranscriber(final Configuration configuration) throws IOException {
		this(configuration, null);
	}


	/**
	 * @param configuration
	 *            The configuration object to be used in order to create the recognizer.
	 * @param path
	 *            The path to the base XML configuration file to be used. If null, then Sphinx relies on the <code>default.config.xml</code> file within its jar.
	 * @throws IOException
	 *             If an error occurred when trying to load the configuration
	 */
	public SphinxTranscriber(final Configuration configuration, final String path) throws IOException {
		this.log = LogFactory.getLog(this.getClass());

		if (path == null && System.getProperty(SphinxTranscriber.JAVA_UTIL_LOGGING_CONFIG_FILE) == null) {
			this.log.debug("Mute Sphinx by setting an empty JUL config. This could either be updated through standard " + JAVA_UTIL_LOGGING_CONFIG_FILE
					+ " option, or through XML configuration of Sphinx." + ".");
			System.setProperty(SphinxTranscriber.JAVA_UTIL_LOGGING_CONFIG_FILE, "");
		}

		final Context context;
		if (path == null) {
			context = new Context(configuration);
		} else {
			context = new Context(path, configuration);
		}

		this.recognizer = new ConfigurableSpeechRecognizer(context);
	}


	/**
	 * @param audioStream
	 *            The audio stream to be listen
	 * @param includeFillers
	 *            Whether or not to retrieve fillers informations in the transcribed result.
	 * @return A list of sentences, i.e. themselves being list of word result
	 */
	public synchronized List<List<WordResult>> transcribe(final InputStream audioStream, final boolean includeFillers) {
		final List<List<WordResult>> returnedWordResultList = new LinkedList<>();
		try {
			this.recognizer.startRecognition(audioStream);
			SpeechResult sResult;
			while ((sResult = this.recognizer.getResult()) != null) {
				final List<WordResult> timedBestResult = sResult.getResult().getTimedBestResult(includeFillers);
				if (timedBestResult.isEmpty()) {
					this.log.debug("Empty sentence retrieved. Skipping it.");
				} else {
					returnedWordResultList.add(timedBestResult);
					final int nbWords = timedBestResult.size();
					this.log.debug("New segment with " + nbWords + " words, ending at " + timedBestResult.get(nbWords - 1).getTimeFrame().getEnd() + " ms.");
				}
			}
		} finally {
			this.recognizer.stopRecognition();
		}
		return returnedWordResultList;
	}

}
