/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */


package org.ow2.weblab.service.transcript.sphinx;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.UnexpectedException;


/**
 * Abstract class to be extended when customising the way the WebLab document is enriched from the Sphinx output.
 *
 * @author ymombrun
 * @date 2015-01-20
 */
public abstract class ATranscriptCreator {


	protected final Log log;


	protected final SphinxTranscriber sphinxTranscriber;


	protected final ContentManager contentManager;


	/**
	 * @param sphinxTranscriber
	 *            The class in charge of really calling Sphinx and handling its results.
	 */
	public ATranscriptCreator(final SphinxTranscriber sphinxTranscriber) {
		this.log = LogFactory.getLog(this.getClass());
		this.sphinxTranscriber = sphinxTranscriber;
		this.contentManager = ContentManager.getInstance();
	}


	/**
	 * @param document
	 *            The input document that should be enriched with text section and where to found MediaUnit with files to be transcribed.
	 * @throws ContentNotAvailableException
	 *             If one of the MediaUnit has no working content available.
	 * @throws UnexpectedException
	 *             If something wrong happens when processing the audio files.
	 */
	public void transcribeDocument(final Document document) throws ContentNotAvailableException, UnexpectedException {
		final Map<MediaUnit, File> mediaUnits = this.retrieveMapOfMediaUnit(document);

		for (final Entry<MediaUnit, File> entry : mediaUnits.entrySet()) {
			this.transcribeFile(entry.getValue(), entry.getKey(), document);
		}
	}


	/**
	 * @param document
	 *            The received document that contains a link to the actual audio file(s) to be processed by Sphinx and that has to be enriched.
	 * @return A <code>Map</code> with <code>Audio</code> or <code>Video</code> <code>MediaUnit</code> associated with the actual file(s) to process.
	 * @throws ContentNotAvailableException
	 *             If an error occurred when trying to access one audio file.
	 */
	protected abstract Map<MediaUnit, File> retrieveMapOfMediaUnit(final Document document) throws ContentNotAvailableException;


	/**
	 * @param audioFile
	 *            The audio file to be analysed by Sphinx
	 * @param mediaUnit
	 *            The <code>MediaUnit</code> that could be annotated to link with the extracted information.
	 * @param document
	 *            The <code>Document</code> that might be enriched with new <code>Text MediaUnit</code> for instance.
	 * @throws UnexpectedException
	 *             If an error occurred when calling Sphinx
	 */
	protected abstract void transcribeFile(final File audioFile, final MediaUnit mediaUnit, final Document document) throws UnexpectedException, ContentNotAvailableException;


}