/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.transcript.sphinx;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;

/**
 * This class is the WebService interface realisation for Sphinx Transcription within the WebLab architecture.
 * It is common to any specific implementation/configuration depending on the language, the model and/or the format of the WebLab document in use.
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class SphinxTranscriptorService implements Analyser {


	protected final Log log;


	protected final ATranscriptCreator transcriptCreator;


	/**
	 * The constructor for Sphinx Transcriptor service.
	 *
	 * @param transcriptCreator
	 *            The class in charge of converting from Sphinx specific format to WebLab specific format
	 */
	public SphinxTranscriptorService(final ATranscriptCreator transcriptCreator) {
		this.log = LogFactory.getLog(this.getClass());
		this.transcriptCreator = transcriptCreator;
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws ContentNotAvailableException, InvalidParameterException, UnexpectedException {
		this.log.trace("Process method called.");

		final Document document = this.checkArgs(args);

		this.log.debug("Process method called for Document '" + document.getUri() + "'.");

		this.transcriptCreator.transcribeDocument(document);

		this.log.debug("End of process method for Document '" + document.getUri() + "'.");

		final ProcessReturn processReturn = new ProcessReturn();
		processReturn.setResource(document);
		return processReturn;
	}


	/**
	 * @param processArgs
	 *            The processArgs; i.e. a usageContext not used and a Resource must be a Document
	 * @throws InvalidParameterException
	 *             If processArgs is null; or if resource is null; or if resource not a document.
	 */
	private Document checkArgs(final ProcessArgs processArg) throws InvalidParameterException {
		if (processArg == null) {
			final String message = "ProcessArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		final Resource res = processArg.getResource();
		if (res == null) {
			final String message = "Resource in ProcessArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		if (!(res instanceof Document)) {
			final String message = "Resource in ProcessArgs was not an instance of Document but of [" + res.getClass().getCanonicalName() + "].";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		return (Document) res;
	}

}
